# [Docker 代理脱坑指南](https://www.cnblogs.com/michael9/p/12300824.html)

# Docker 代理配置

由于公司 Lab 服务器无法正常访问公网，想要下载一些外部依赖包需要配置公司的内部代理。Docker 也是同理，想要访问公网需要配置一定的代理。

Docker 代理分为两种，一种是为运行的 Container 配置代理，用于下载一些依赖包以及访问公网。另一种是为 Docker Daemon 配置代理，用于支持 docker 相关的命令。

## 为容器配置代理[#](https://www.cnblogs.com/michael9/p/12300824.html#为容器配置代理)

配置容器代理一般分为两种，一种是全局配置，另一种是仅为某个容器配置。

### 全局配置[#](https://www.cnblogs.com/michael9/p/12300824.html#全局配置)

首先说明，此修改方法仅支持 17.07 或者更高版本。

修改或创建 `~/.docker/config.json`

```
# 如果有的话，先备份一下
cp ~/.docker/config.json ~/.docker/config.json.bk

# 修改内容如下
cat ~/.docker/config.json
{
        "auths": {},
        "HttpHeaders": {
                "User-Agent": "Docker-Client/19.03.2 (linux)"
        },

		"proxies":
 		{
   			"default":
   			{
     			"httpProxy": "http://173.39.112.117:80",
     			"httpsProxy": "http://173.39.112.117:80"
   			}
 		}
}
```

为了确保生效，重启下 docker :`systemctl restart docker`

此时宿主机并没配置代理，查询下 IP:

```
[root@localhost ~]# curl cip.cc
IP      : 64.104.xxx.xx
地址    : 中国  香港  cisco.com

数据二  : 香港 | 特别行政区

数据三  : 中国香港

URL     : http://www.cip.cc/64.104.xxx.xx
```

基于之前[使用 Docker file 打包镜像](https://www.cnblogs.com/michael9/p/12303748.html#_label2)的文章，直接使用打包好带有 systemd 功能的镜像。

```
# 创建 container
[root@localhost home] docker run --privileged=true -ti \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
-p 80:80 -d local/c7-systemd

# 进入 container
[root@localhost home] docker exec -it 3eaa1cc71706 /bin/bash

# 查询 IP
[root@3eaa1cc71706 /]# curl cip.cc
IP      : 173.39.112.xxx
地址    : 新加坡  新加坡

数据二  : 新加坡

数据三  : 新加坡

URL     : http://www.cip.cc/173.39.112.xxx
```

可以看到容器内已经成功配置了代理，可以正常下载依赖了。

### 局部修改[#](https://www.cnblogs.com/michael9/p/12300824.html#局部修改)

方法1-在 docker run 命令添加参数

```
# 创建 container
docker run --privileged=true -ti \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
--env HTTP_PROXY="http://173.39.112.117:80 \
--env HTTPS_PROXY="http://173.39.112.117:80 \
--env http_proxy="http://173.39.112.117:80 \
--env https_proxy="http://173.39.112.117:80 \
-p 80:80 -d local/c7-systemd

# 进入 container
[root@localhost home]# docker exec -it 3607976e8f2d /bin/bash

# 查询 IP
[root@3607976e8f2d /]# curl cip.cc
IP      : 173.39.112.xxx
地址    : 新加坡  新加坡

数据二  : 新加坡

数据三  : 新加坡

URL     : http://www.cip.cc/173.39.112.xxx
```

方法2-在 Docker-file 添加

这里以打包一个 httpd 的 docker file 为例子：

```shell
FROM local/c7-systemd

ENV MY_PROXY_URL="http://173.39.112.117:80"
ENV HTTP_PROXY=$MY_PROXY_URL \
    HTTPS_PROXY=$MY_PROXY_URL \
    FTP_PROXY=$MY_PROXY_URL \
    http_proxy=$MY_PROXY_URL \
    https_proxy=$MY_PROXY_URL \
    ftp_proxy=$MY_PROXY_URL


RUN yum -y install httpd; yum clean all; systemctl enable httpd.service

EXPOSE 80

CMD ["/usr/sbin/init"]
```

结果是相同的，这里就不演示了。有时添加代理是域名的话，就需要额外的操作。

### 添加代理是域名的处理[#](https://www.cnblogs.com/michael9/p/12300824.html#添加代理是域名的处理)

如果添加的代理是域名的话，如 `proxy.esl.cisco.com:80`, 需要再做一步额外的处理。

方法1-通过 docker run 参数添加

```
# 创建 container
[root@localhost home]#docker run --privileged=true -ti \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
--env HTTP_PROXY="http://proxy.esl.cisco.com:80 \
--env HTTPS_PROXY="http://proxy.esl.cisco.com:80 \
--env http_proxy="http://proxy.esl.cisco.com:80 \
--env https_proxy="http://proxy.esl.cisco.com:80 \
--dns=64.104.123.245 \
-p 80:80 -d local/c7-systemd

# 进入 container
[root@localhost home]# docker exec -it 992dc27de1cc /bin/bash

# 查看 IP
[root@992dc27de1cc /]# curl cip.cc
IP      : 173.39.xxx.xxx
地址    : 新加坡  新加坡

数据二  : 新加坡

数据三  : 新加坡

URL     : http://www.cip.cc/173.39.xxx.xxx
```

方法2-通过修改 docker daemon 配置添加

在每个 container 运行前，会继承 Docker daemon 的配置，在 `/etc/docker/daemon.json` 文件下.

```
# 为 docker daemon 添加 dns，在运行时会为每个 container 添加上
cat /etc/docker/daemon.json
{
  "dns" : [
    "8.8.4.4",
    "8.8.8.8",
    "Your_DNS_SERVER"
  ],

  "registry-mirrors":["https://docker.mirrors.ustc.edu.cn"]
}
```

效果一样这里就不演示了。

## 为 Docker Daemon 添加代理[#](https://www.cnblogs.com/michael9/p/12300824.html#为-docker-daemon-添加代理)

和 container 的情况一样，如果不为 Docker Daemon 配置代理的话，是无法使用 search, pull, push 等命令的。

配置如下：

```
# STEP1-创建文件夹
[root@localhost home]# sudo mkdir -p /etc/systemd/system/docker.service.d

# STEP2-创建代理文件 http 和 https
[root@localhost home]# cat /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://proxy.esl.cisco.com:80/"

[root@localhost home]# cat /etc/systemd/system/docker.service.d/https-proxy.conf
[Service]
Environment="HTTPS_PROXY=http://proxy.esl.cisco.com:80/"

# 如果希望访问某些 Docker registries 不是用代理，可以在上面的配置文件后追加
[Service]    
Environment="HTTP_PROXY=http://proxy.example.com:80/" "NO_PROXY=localhost,127.0.0.1,docker-registry.example.com,.corp"

# STEP3-刷新变更
[root@localhost home]# sudo systemctl daemon-reload

# STEP4-重启 Docker
[root@localhost home]# sudo systemctl restart docker

# STEP5-验证代理是否生效
[root@localhost home]# systemctl show --property=Environment docker
Environment=HTTP_PROXY=http://proxy.esl.cisco.com/ HTTPS_PROXY=http://proxy.esl.cisco.com:80/
```

## 参考[#](https://www.cnblogs.com/michael9/p/12300824.html#参考)

[docker-container-proxy](https://docs.docker.com/network/proxy/#configure-the-docker-client)

[docker-dns](https://docs.docker.com/config/containers/container-networking/)

[docker-daemon-proxy](https://docs.docker.com/config/daemon/systemd/)