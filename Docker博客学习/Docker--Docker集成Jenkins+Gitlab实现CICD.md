# [Docker 集成 Jenkins Gitlab 实现 CI/CD](https://www.cnblogs.com/michael9/p/12921363.html)

首先介绍下环境部分，文章中共涉及到三台服务器，分别用 Gitlab，Jenkins，Deploy 三个名称代替，部署在内网环境，同时因为政策原因，服务器无法直接连通外网。下载 Jenkins 插件时需要添加代理，如服务器可直接联通外网，忽略即可。

其中服务器详细信息如下：

| 服务器名称 | 服务器 IP             | 作用               |
| ---------- | --------------------- | ------------------ |
| Gitlab     | http://10.124.207.51/ | 代码管理           |
| Jenkins    | http://10.124.205.60/ | Jenkins 所在服务器 |
| Deploy     | http://10.124.205.69/ | UTC 部署环境       |

实现功能：

Jenkins 通过监听 GItlab 分支变化，实时更新代码，并将编译后的代码部署在 UTC 环境。

实现思路：

1. 在 Jenkins 所在服务器，使用 docker 安装 Jenkins，启动并下载所需插件。

2. 通过 Jenkins 提供的管理页面，创建管理员用户。并配置所需凭证。

   关于凭证这里详细说下，共需要配置如下凭证：

   - 配置用于访问 Gitlab 的凭证
   - 配置用于拉取 Gitlab 仓库代码的凭证
   - 配置用于访问 UTC 部署环境的凭证

3. 创建任务，将三者连接起来。

## Jenkins 安装[#](https://www.cnblogs.com/michael9/p/12921363.html#jenkins--安装)

这里 docker 安装就不演示了，相关配置可参考[这篇](https://www.cnblogs.com/michael9/p/12787873.html)，使用 docker pull 镜像：

```
docker pull jenkinsci/blueocean
```

运行 Jenkins ：

```
docker run \
  -u root \
  -d \
  -p 50001:8080 \
  -p 50000:50000 \
  -v /etc/localtime:/etc/localtime \
  -v jenkins-data:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --name jenkins \
  jenkinsci/blueocean
```

## Jenkins 插件安装[#](https://www.cnblogs.com/michael9/p/12921363.html#jenkins-插件安装)

在浏览器输入 [http://10.124.205.60:50001](http://10.124.205.60:50001/) 进入 jenkins 管理页面，之后会提示输入密码。

jenkins 的初始登录密码可进入 docker 所在的容器中，可以通过 `cat /root/.jenkins/secrets/initialAdminPassword` 获取。

### 代理配置[#](https://www.cnblogs.com/michael9/p/12921363.html#代理配置)

由于公司 ACL 的限制，无法直接访问外网，这时会在页面上提示，当前 Jenkins 为离线版本，故需要配置代理：

```
在服务器输入框中输入代理地址：proxy.esl.cisco.com
端口：80
测试 URL：用于测试代理是否配置成功。
在下载插件后，可以将代理清掉，以免在内网访问时出现问题。
```

[![img](image/1861307-20200520082318381-146566102.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082318381-146566102.png)

### 插件安装[#](https://www.cnblogs.com/michael9/p/12921363.html#插件安装)

在配置代理成功后，会提示安装插件，这里选择推荐的插件选项安装。

登录成功后，由于 Jenkins 自带的插件中，没有 Gitlab 和 Publish Over SSH ，需要我们手动安装下，在 `系统管理/插件管理/可选插件`目录下 ，搜索关键字即可。其中 `GitLab Plugin` 用于对 Gitlab 的访问，`Publish Over SSH` 用于传输文件以及在目标服务器上执行命令。

## 凭证配置[#](https://www.cnblogs.com/michael9/p/12921363.html#凭证配置)

### 配置 Gitlab[#](https://www.cnblogs.com/michael9/p/12921363.html#配置-gitlab)

在 `系统管理/系统配置`页面，向下滑动，找到 gitlab 的选项，该配置用于访问 Gitlab.

```
Connection name: 随便起一个连接名称
Gitlab host URL： 为 Gitlab 的地址
Credentials：选择一个认证的方式。因为咱们是第一次，选择添加，类型为 Gitlab API token. 将 gitlab 的 token 粘贴到 API token 中。接着点击添加。最后通过，点击测试判断是否正常。
```

[![img](image/1861307-20200520082432504-868171740.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082432504-868171740.png)

[![img](image/1861307-20200520082527326-1598078215.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082527326-1598078215.png)

如果不知道如何获取 gitlab 的 token，可以参考下图，在 Gitlab 管理页面创建：

[![img](image/1861307-20200520082616607-724718591.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082616607-724718591.png)

### Git 配置[#](https://www.cnblogs.com/michael9/p/12921363.html#git-配置)

向下滑动，找到 git plugin 配置，配置上用户名和邮箱即可：

[![img](image/1861307-20200520082643926-2111538651.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082643926-2111538651.png)

### Publish over SSH 配置[#](https://www.cnblogs.com/michael9/p/12921363.html#publish-over-ssh-配置)

该插件的作用主要是通过 SSH 命令，操作目标服务器，实现自动部署。

SSH 连接一般有两种方式，用户名密码或公私匙的方式，下面用公私匙的方式操作：

```
# 首先进入 Jenkins 所在的 docker 容器
 docker exec -it jenkins /bin/bash
 
# 生成公私匙，需要注意的是新版本的格式 Jenkins 插件不支持，请使用 PEM 这种格式
ssh-keygen -t rsa -b 4096 -m PEM

# 将公匙拷贝至目标服务器 
ssh-copy-id -i .ssh/id_rsa.pub root@10.124.205.69
```

下面开始配置 Jenkins：

```
Passphrase: 这个是在生成 ssh key 时，输入的密码，不填写就行。
Path to key: 这个是 ssh 私匙文件所在的绝对位置，当填写下面的 Key 不填写即可。
Key：生成的私匙密码
SSH Server 配置：
Name：随便起名就可以
Username：ssh 登录的用户名
Remote Directory：这个一定要填写根路径，之后的操作都会基于该路径。

可点击 `Test Configuration` 是否配置成功，点击应用，保存即可。
```

[![img](image/1861307-20200520082713089-1492305153.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082713089-1492305153.png)

## Jenins Job 配置[#](https://www.cnblogs.com/michael9/p/12921363.html#jenins-job-配置)

在首页上，点击新建任务。输入任务名称，类型选择第一个，构建一个自由风格的软件项目，点击确定，任务就创建完成了。

然后将鼠标放在名称这列，可以看到一个小三角，点击展开后，对任务进行配置：

[![img](image/1861307-20200520082734062-1707053159.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082734062-1707053159.png)

### 源码配置[#](https://www.cnblogs.com/michael9/p/12921363.html#源码配置)

选择源码管理，选择 Git，用于获取源代码。这里简单提一下，之前配置的 Gitlab 用于对 Gitlab 的访问，而这里配置的 GIt 用于对某个仓库进行访问。

这里需要将之前生成 ssh 公匙放到 Gitlab 的 SSH Keys 中，将 ssh 私匙放在当前的 key 中。用于拉取代码。
[![img](image/1861307-20200520082755252-2002135447.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082755252-2002135447.png)

将公匙放入 Gitlab 中：

[![img](image/1861307-20200520082849154-1628803317.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082849154-1628803317.png)

接着点击应用和保存。

### 构建触发器的配置[#](https://www.cnblogs.com/michael9/p/12921363.html#构建触发器的配置)

触发器的作用就是在什么情况下，会开始执行该任务。这里配置的是轮询 SCM 系统，也就是 Gitlab 分支的内容出现变化时，触发任务，轮询的时间为 1s.

[![img](image/1861307-20200520082909222-1356887989.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082909222-1356887989.png)

### 构建配置[#](https://www.cnblogs.com/michael9/p/12921363.html#构建配置)

构建的作用在获取 SCM 上的代码后，执行的操作：

```
执行 shell: 注意这里编写的 shell 的运行环境是在 jenkins 所在容器内部的。
```

选择添加 `Sendfiles or execute commands over SSH`：

```
Name: 这里选择，之前建好用于访问目标服务器的凭证
Source files: **/*.* 表示递归传输文件夹下的包含 . 的所有文件
Remote directory: 表示传送的目标目录
Exec command: 表示所有文件传输成功后，在目标服务器执行的 shell 命令。
```

[![img](image/1861307-20200520082931540-1109979083.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082931540-1109979083.png)

下面是服务器中执行的脚本，用于编译代码以及重启服务。

```
# 注意添加可执行权限
chmod +x deploy_shell.sh
cat deploy_shell.sh

#!/bin/bash

# constant
containerName="ctg_backend"
WORKING_DIR="/home/yuwzhang/docker-compose/project/temp_build"
TODAY=`date +%Y-%m-%d.%H:%M:%S`

echo "Enter deploy script..."


echo "\nStep1: Copy data to docker web container.."
# delete old dir
if [ -d "$WORKING_DIR" ]; then rm -Rf $WORKING_DIR; fi
# cp new dir
cp -r /home/yuwzhang/jenkins/project /home/yuwzhang/docker-compose/project/temp_build


echo "\nStep2: Enter web container and build py files to bytecode.."
docker exec $containerName bash -c "cd /src/temp_build  && python -m compileall -b . && find . -name '*.py' |xargs rm -rf && find . -name 'pycache' |xargs rm -rf"
cp /home/yuwzhang/docker-compose/project/gunicorn_config.py /home/yuwzhang/docker-compose/project/temp_build/gunicorn_config.py
cp -r /home/yuwzhang/docker-compose/project/frontend /home/yuwzhang/docker-compose/project/temp_build/frontend


echo "\nStep3: Stop web services.."
cd /home/yuwzhang/docker-compose
docker-compose stop web


echo "\nStep4: replace new project dir to old project dir.."
mv project/temp_build new_build
mv project project_`date +%Y-%m-%d.%H:%M:%S`
mv new_build project


echo "\nStep5: Start web services.."
docker-compose start web


echo "\nStep6: check web services status.."
sleep 20s
exist=`docker inspect --format '{{.State.Status}}' ${containerName}`
echo "web status is $exist"
```

现在，一个基础版的 CI/CD 流程就搭建完了，可以通过 Blue Ocean 来观察任务的执行情况：

[![img](image/1861307-20200520082956582-571094005.png)](https://img2020.cnblogs.com/blog/1861307/202005/1861307-20200520082956582-571094005.png)

### 设置时间[#](https://www.cnblogs.com/michael9/p/12921363.html#设置时间)

在运行 Jenkins 容器时，已经挂载了宿主机的时间到容器内，可是发现 Jenkins 显示的时间还是不正确，在容器内查看时间是正常的，也许和 Jenkins 的配置有关，在`系统管理/脚本命令行`中执行如下命令，时间就恢复正常了。

```
System.setProperty('org.apache.commons.jelly.tags.fmt.timeZone', 'Asia/Shanghai')
```

## 参考[#](https://www.cnblogs.com/michael9/p/12921363.html#参考)

[Jenkins Book](https://www.jenkins.io/zh/doc/book/installing/)